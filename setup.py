#!python3

from distutils.core import setup

setup(name='hmm',
      version='1.0',
      description='HMM by GuyZ, originally at https://bitbucket.org/GuyZ/hmm',
      author='GuyZ',
      url='https://bitbucket.org/GuyZ/hmm',
      packages=['hmm','hmm.continuous','hmm.continuous','hmm.discrete','hmm.examples','hmm.weights'],
     )
